import React from 'react';

class App extends React.Component {

	constructor() {

		super();
		this.state = {
			data: "",
			hashPost: [],
			table_info: [],
			rowcount : 0
		};
		this.hashPosts = [
			{
			"Post" : "Posts",
			"Like" : "Likes",	
			"Angry" : "Angry",	
			"Cry" : "Cry",	
			"Haha" : "Haha",	
			}];

		this.setPostList = this.setPostList.bind(this);
		this.deletePost = this.deletePost.bind(this);
		this.setEmotionPost = this.setEmotionPost.bind(this);
	};

	setPostList() {

		var postVal = document.getElementById("my_post").value;

		console.log("POSTVAL : "+postVal)
		var hash_cont = {};
		hash_cont.Post = postVal;
		hash_cont.Like = 0;
		hash_cont.Angry = 0;
		hash_cont.Cry = 0;
		hash_cont.Haha = 0;

		var id_key = {};
		var length_key = 0;
		var my_array = [];

		if (this.state.hashPost) {
			length_key = this.state.hashPost.length;
			hash_cont.post_id = length_key;
			my_array = this.state.hashPost;
			my_array.push(hash_cont)
			this.state.hashPost = my_array;
			this.setState({'rowcount' : length_key})

		} else {
			length_key = 0;
			hash_cont.post_id = 0;
			var my_array = []
			my_array.push(hash_cont);
			this.state.hashPost.append(hash_cont)
			this.setState({'rowcount' : length_key})
		}

		console.log("LENGTH : "+this.state.hashPost.length)
		for (var i = 0 ; i < this.state.hashPost.length ; i++) {
			console.log(">>>>"+this.state.hashPost[i].Post)
			console.log(">>>>"+this.state.hashPost[i].post_id)
		}

	}

	deletePost(row_id,e) {

		var temp_array = this.state.hashPost

		for (var i = 0; i < this.state.hashPost.length; i++) {
			var temp_id = this.state.hashPost[i].Post+"_"+this.state.hashPost[i].post_id;

			if (row_id == temp_id) {
				temp_array.splice(i, 1);

				this.setState({hashPost : temp_array})

				return

			}

		}		

	}

	setEmotionPost(row_id,key) {


		console.log("UPDATE EMOTION : "+row_id)
		console.log("UPDATE EMOTION : "+key)

		for (var i = 0; i < this.state.hashPost.length; i++) {
			var temp_id = this.state.hashPost[i].Post+"_"+this.state.hashPost[i].post_id;

			if (row_id == temp_id) {

				if (key == 'Like') {
					this.state.hashPost[i].Like = this.state.hashPost[i].Like + 1;
				} else if (key == 'Angry') {
					this.state.hashPost[i].Angry = this.state.hashPost[i].Angry + 1;
				} else if (key == 'Cry') {
					this.state.hashPost[i].Cry = this.state.hashPost[i].Cry + 1;
				} else if (key == 'Haha') {
					this.state.hashPost[i].Haha = this.state.hashPost[i].Haha + 1;
				}

				var temp_array = this.state.hashPost
				this.setState({hashPost : temp_array})

				return
			}

		}		

	}

	render() {

		var retVar = []
		for (var i = 0 ; i < this.state.hashPost.length; i++) {
			console.log("PROPS Post : "+this.state.hashPost[i].Post)
			var del_id = this.state.hashPost[i].Post+"_"+this.state.hashPost[i].post_id;
			retVar.push(<tr>
				<td>{this.state.hashPost[i].Post}</td>
				<td>{this.state.hashPost[i].Like}</td>
				<td>{this.state.hashPost[i].Angry}</td>
				<td>{this.state.hashPost[i].Cry}</td>
				<td>{this.state.hashPost[i].Haha}</td>
				<td><button id={del_id} onClick={this.deletePost.bind(this,del_id)}>Delete</button></td>
				<td><button id={del_id} onClick={this.setEmotionPost.bind(this,del_id,'Like')}>Like</button></td>
				<td><button id={del_id} onClick={this.setEmotionPost.bind(this,del_id,'Angry')}>Angry</button></td>
				<td><button id={del_id} onClick={this.setEmotionPost.bind(this,del_id,'Cry')}>Cry</button></td>
				<td><button id={del_id} onClick={this.setEmotionPost.bind(this,del_id,'Haha')}>Haha</button></td>
				</tr>);
		}

		return (
			<div>
				<h1> POST LIST </h1>
				<input type='text' id="my_post" />
				<button name='Post' value='Post' id='clickbutton' onClick={this.setPostList}> Post </button>

				{/*<TableInformationB hashPost = {this.state.hashPost}></TableInformationB>*/}
				<table>
					<tbody>
						<tr>
							<td> POSTS </td>
							<td> Like: </td>
							<td> Angry: </td>
							<td> Cry: </td>
							<td> Haha: </td>
						</tr>{retVar}</tbody>
				</table>
			</div>
		);
	}
}

class TableInformationB extends React.Component {


	render() {
		if (this.props.hashPost.length == 0) {
			return (
				<table></table>
			);
		}

		console.log("LENGTH__ : "+this.props.hashPost.length)
		var retVar = []
		for (var i = 0 ; i < this.props.hashPost.length; i++) {
			console.log("PROPS Post : "+this.props.hashPost[i].Post)
			var del_id = this.props.hashPost[i].Post+"_"+this.props.hashPost[i].post_id;
				retVar.push(<tr>
					<td>{this.props.hashPost[i].Post}</td>
					<td>{this.props.hashPost[i].Like}</td>
					<td>{this.props.hashPost[i].Angry}</td>
					<td>{this.props.hashPost[i].Cry}</td>
					<td>{this.props.hashPost[i].Haha}</td>
					<td><button id={del_id} onClick={this.deletePost}>Delete</button></td>
					</tr>);
		}
		console.log("RETURN : "+retVar)
		return (
			<table>
				<tbody>
					<tr>
						<td> POSTS </td>
						<td> Like: </td>
						<td> Angry: </td>
						<td> Cry: </td>
						<td> Haha: </td>
					</tr>{retVar}</tbody>
			</table>
		);
	}
}

export default App;
